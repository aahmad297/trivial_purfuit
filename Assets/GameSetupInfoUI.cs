﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSetupInfoUI : MonoBehaviour
{
  public Text promptText;
  public InputField playerNameField;
  public Button[] colorButtons;
  public Color colorSelected;
  public int colorSelectedNum;


  public void InitializeScreen(int playerNum, bool[] colorsUsed)
  {
    promptText.text = "Enter Player " + playerNum.ToString() + "'s Name and Select Color";
    
    for (int i = 0; i<4; i++)
    {
      if (colorsUsed[i])
      {
        colorButtons[i].gameObject.SetActive(false);
      } 
    }

    colorButtons[0].onClick.AddListener(delegate { GetColor(0); });
    colorButtons[1].onClick.AddListener(delegate { GetColor(1); });
    colorButtons[2].onClick.AddListener(delegate { GetColor(2); });
    colorButtons[3].onClick.AddListener(delegate { GetColor(3); });

  }

  private void GetColor(int numIn)
  {
    if (playerNameField.text != "")
    {
      colorSelectedNum = numIn;
      colorSelected = colorButtons[numIn].GetComponent<Image>().color;
    }
  }
}
