﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine;

public class GameHandlerFileHelper : MonoBehaviour
{
  // Define constants
  private const char DELIMITER = ',';
  private const string TYPE = "type";
  private const string NAME = "name";
  private const string COLOR = "color";
  private const string QUESTION = "question";
  private const string ANSWER = "answer";

  // Define member variables for storage
  private e_question_type categoryNum;
  private List<QuestionCard> questionDeck;
  private Dictionary<e_question_type, string> categoryNames;
  private List<Color> categoryColors;

  // Get a list of the question cards
  public bool ReadQuestionDatabase(string filePath)
  {
    // Need variables to help read the CSV file, base for the new deck, and base for category/colors
    bool returnValue = true;
    Color qColorTemp;
    int qCardNum = 1;
    QuestionCard qCardTemp = null;
    string[] database;
    string[] contents;
    string qCatTemp;

    // Read in the file contents and initialize variables
    categoryColors = new List<Color>();
    categoryNames = new Dictionary<e_question_type, string>();
    questionDeck = new List<QuestionCard>();
    database = File.ReadAllLines(filePath);

    // Parse each line
    foreach (string line in database)
    {
      // Split the contents based on delimiter
      contents = line.Split(DELIMITER);

      // Switch statement based on first line - handle them differently
      // TODO: Currently colors/category names are information only
      switch (contents[0].ToLower())
      {
        // Do nothing for the "Type" line, ignoring.
        case TYPE:
          break;
        
        // Log the colors and categories to the debug output
        case COLOR:
          for (int i = 1; i < contents.Length; i++)
          {
            qColorTemp = ParseColorHex(contents[i]);
            categoryColors.Add(qColorTemp);
            UnityEngine.Debug.Log("Category " + i + "'s " + contents[0].ToLower() + " is " + qColorTemp.ToString());
          }
          break;

        case NAME:
          for (int i = 1; i < contents.Length; i++)
          {
            categoryNames.Add((e_question_type)(i - 1), contents[i]);
            categoryNames.TryGetValue((e_question_type)(i - 1), out qCatTemp);
            UnityEngine.Debug.Log("Category " + i + "'s " + contents[0].ToLower() + " is " + qCatTemp);
          }
          break;

        // Question comes before answers, so create a new card and set the questions
        case QUESTION:
          qCardTemp = ScriptableObject.CreateInstance<QuestionCard>();
          for (int i = 1; i < contents.Length; i++)
          {
            categoryNames.TryGetValue((e_question_type)(i - 1), out qCatTemp);
            qCardTemp.SetCategoryByType((e_question_type)(i - 1), qCatTemp);
            qCardTemp = ParseQuestionAnswer(contents, qCardTemp, QUESTION);
            UnityEngine.Debug.Log("Question #" + qCardNum + "'s category " + i + " " + contents[0].ToLower() + " is " + contents[i]);
          }
          break;

        // Answer comes after the questions, so add the card to the deck!
        case ANSWER:
          if (qCardTemp != null)
          {
            for (int i = 1; i < contents.Length; i++)
            {
              qCardTemp = ParseQuestionAnswer(contents, qCardTemp, ANSWER);
              UnityEngine.Debug.Log("Question #" + qCardNum++ + "'s category " + i + " " + contents[0].ToLower() + " is " + contents[i]);
            }
            questionDeck.Add(qCardTemp);
          }
          else
          {
            UnityEngine.Debug.LogError("ERROR: Question must come before the answer!");
          }
          break;

        // Not matching any of the above is the error case
        default:
          UnityEngine.Debug.LogError("ERROR: Row doesn't match the standard! Please review the Question Database!");
          returnValue = false;
          break;
      }
    }

    return returnValue;
  }
  
  // Standard get method for the read-in question card deck
  public List<QuestionCard> GetQuestionDeck()
  {
    return questionDeck;
  }

  // Standard get method for the read-in category colors
  public List<Color> GetCategoryColors()
  {
    return categoryColors;
  }

  // Standard get method for the read-in category naames
  public string GetCategoryNames(e_question_type category)
  {
    categoryNames.TryGetValue(category, out string catName);
    return catName;
  }

  // Helper method for parsing questions and answers
  private QuestionCard ParseQuestionAnswer(string[] contents, QuestionCard qCard, string type)
  {
    for (int i = 1; i < contents.Length; i++)
    {
      categoryNum = (e_question_type)(i - 1);

      if (type.Equals(QUESTION))
      {
        qCard.SetQByType(categoryNum, contents[i]);
      }
      else if (type.Equals(ANSWER))
      {
        qCard.SetAByType(categoryNum, contents[i]);
      }
      else
      {
        UnityEngine.Debug.LogError("Error Parsing Question/Database: First colum expected to be Question/Answer " + type + " received.");
      }
    }

    return qCard;
  }

  // Helper method for parsing Category Colors, converting from hex to float to create a valid color
  private Color ParseColorHex(string contents)
  {
    bool success;

    // ColorUtility allows parsing an HTML string. Usage:
    //    - #RGB becomes RRGGBB
    //    - #RRGGBB
    //    - #RGBA becomes RRGGBBAA
    //    - #RRGGBBAA
    //    - Can enter the following if string does not begin with '#'
    //        - red, cyan, blue, darkblue, lightblue, purple, yellow, lime, fuchsia, white, 
    //          silver, grey, black, orange, brown, maroon, green, olive, navy, teal, aqua, magenta
    success = ColorUtility.TryParseHtmlString(contents, out Color parsedColor);

    if (!success)
    {
      UnityEngine.Debug.LogError("Error Parsing Color: Color " + contents + " is invalid. Please Enter in #hex format of from the list of available colors.");
    }

    return parsedColor;
  }
}
