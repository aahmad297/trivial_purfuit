﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandlerUtilities
{
  // Simulate rolling a 6-sided die with 
  public static int RollDice(int numSides)
  {
    // Set the min and max values (max being max + 1)
    int minValue = 1;
    int maxValuePlusOne = numSides + 1;

    // Use System.Random with a semi-random seed. Need to use System to avoid Unity's random class.
    System.Random dice = new System.Random(Guid.NewGuid().GetHashCode());

    UnityEngine.Debug.Log("GameHandler notified by RollDiceUI that user clicked the roll dice button.");

    return dice.Next(minValue, maxValuePlusOne);
  }

  // This function translates the current player's position top the tile type they're on
  public static e_tile_type FindCurrentTileType(PlayerToken[] players, GameBoardHandler gameBoardHandler, int currentPlayer)
  {
    // Define local variables
    int row, col;

    // Get the current player's row/column information
    col = players[currentPlayer].GetCol();
    row = players[currentPlayer].GetRow();

    return gameBoardHandler.GetTileTypeByCoord(row, col);
  }

  // This function creates a dictionary of the question category relating to each HQ or question tile
  public static Dictionary<e_tile_type, e_question_type> MapTilesQuestions()
  {
    // Initialize a brand new dictionary
    Dictionary<e_tile_type, e_question_type> tileQuestionMap = new Dictionary<e_tile_type, e_question_type>();

    // Use a switch statement for easy equivalences
    foreach (e_tile_type tileType in System.Enum.GetValues(typeof(e_tile_type)))
    {
      switch (tileType)
      {
        // HQ and regular category nodes refer to the same categories.
        case e_tile_type.cat_1:
        case e_tile_type.hq_cat_1:
          tileQuestionMap.Add(tileType, e_question_type.cat_1);
          break;

        case e_tile_type.cat_2:
        case e_tile_type.hq_cat_2:
          tileQuestionMap.Add(tileType, e_question_type.cat_2);
          break;

        case e_tile_type.cat_3:
        case e_tile_type.hq_cat_3:
          tileQuestionMap.Add(tileType, e_question_type.cat_3);
          break;

        case e_tile_type.cat_4:
        case e_tile_type.hq_cat_4:
          tileQuestionMap.Add(tileType, e_question_type.cat_4);
          break;

        // Do nothing for the default case
        default:
          break;
      }
    }

    return tileQuestionMap;
  }
}

