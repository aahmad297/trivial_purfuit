﻿using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
  private const string FILE_PATH = "Assets/QuestionDatabase.csv";
  private const int DICE_SIDES = 6;
  private const bool ANS_DEBUG = true;
  private const bool DICE_DEBUG = true;
  private int[] debugDiceRolls = { 4, 4, 2, 4, 4 };
  private int debugDiceSize = 1 - 1;
  private int debugDiceIndx = 0;
  public static readonly int MAX_PLAYERS = 4;

  // Game Board Display and Handler ************************************//
  public GameBoardUI gameBoardUIPrefab;
  private GameBoardUI gameBoardUI;
  private GameBoardHandler gameBoardHandler;
  private e_tile_type currentTile;
  private Dictionary<e_tile_type, e_question_type> tileQuestionMap;
  // Deck Management and Question Display *******************************//
  private GameHandlerFileHelper questionDatabase;
  public QuestionDisplayUI qDisplayUIPrefab;
  private QuestionDisplayUI qDisplay;
  private Deck gameDeck;
  private QuestionCard currentQuestion;
  private e_question_type currentCategory;
  // Dice Rolling  ******************************************************//
  public RollDiceUI rollDiceUIPrefab;
  private RollDiceUI rollDiceUI;
  private int diceResult;
  // Player Answer ******************************************************//
  public PlayerAnswerUI playerAnswerUIPrefab;
  private PlayerAnswerUI playerAnswerUI;
  public PlayerToken playerTokenPrefab;
  // Category Selection *************************************************//
  public CategorySelectUI categorySelectUIPrefab;
  private CategorySelectUI categorySelectUI;
  // Player + Turn Management ******************************************//
  public GameSetupUI gameSetupUIPrefab;
  private GameSetupUI gameSetupUI;
  private int currentPlayer, maxPlayer;
  private PlayerToken[] players; //= new PlayerToken[4];
  private List<Vector2> validMoves;
  private e_game_state gameState;
  private int turnNumber = 0;
  private bool gameWon = false;
  private bool[] winners = new bool[4];
  private bool[] turnDone = new bool[4];
  private bool gameDone = false;
  // Notifications *****************************************************//
  public NotificationDisplay notificationDisplayPrefab;
  private NotificationDisplay notificationDisplay;
  public PopupMessage popupMessagePrefab;
  public EndGamePanel endGamePanelPrefab;
  private EndGamePanel endGamePanel;

  // Start is called before the first frame update
  void Start()
  {
    // Parse questions and init deck
    InitDeck();
    // Create GameBoard UI element
    InitGameBoard();
    // Get player info
    GetPlayerInfo();
    gameState = e_game_state.get_player_info;
  }

  public void InitDeck()
  {
    // Map tiles to their question categories upon startup
    tileQuestionMap = GameHandlerUtilities.MapTilesQuestions();

    // Start by setting up a new deck and reading in the Question Database
    questionDatabase = gameObject.AddComponent<GameHandlerFileHelper>();
    questionDatabase.ReadQuestionDatabase(FILE_PATH);
    gameDeck = new Deck(questionDatabase.GetQuestionDeck());

    // Set up question display UI
    
  }
  public void InitGameBoard()
  {
    gameBoardUI = Instantiate(gameBoardUIPrefab, transform);

    // Create Notification Display ScrollView element
    notificationDisplay = Instantiate(notificationDisplayPrefab, transform);

    // Create the GameBoard subsystem -- it will have static format for board
    gameBoardHandler = gameObject.AddComponent(typeof(GameBoardHandler)) as GameBoardHandler;

    // Set colors in gameBoardHandler if we don't want default
    List<Color> colors = questionDatabase.GetCategoryColors();
    gameBoardHandler.SetColorsForTiles(colors);
    Dictionary<e_tile_type, Color> colorMap = gameBoardHandler.GetColorMap();

    // Get the GameBoardHandler's type array to pass to the UI so it can 
    // create tiles
    e_tile_type[,] TileFormat = gameBoardHandler.GetTypeArray();

    // Set the tileClicked callback function, so we get notified by
    // the UI when any tile is clicked
    gameBoardUI.GetComponent<GameBoardUI>().Initialize(TileClicked);
    // Create the game board, passing in the GameBoard subsystem info
    // the gameBoardUI does not save this info, it just creates the tiles
    gameBoardUI.GetComponent<GameBoardUI>().Populate(TileFormat, colorMap);
    gameBoardHandler.GrabTiles(gameBoardUI.GetBoardTileRef());
  }

  public void GetPlayerInfo()
  {
    gameSetupUI = Instantiate(gameSetupUIPrefab, transform);
    gameSetupUI.SetColors(questionDatabase.GetCategoryColors());
  }

  public void InitPlayerTokens()
  {

    s_player_info[] playerInfo = gameSetupUI.playerInfo;
    maxPlayer = gameSetupUI.numPlayers - 1;
    List<Color> colors = questionDatabase.GetCategoryColors();
    Vector2 newPos;
    Tile startPosTile;
    string startText = "";

    players = new PlayerToken[gameSetupUI.numPlayers];
    for (int i = 0; i < gameSetupUI.numPlayers; i++)
    {
      players[i] = Instantiate(playerTokenPrefab, transform);
      players[i].SetCakeColors(colors);
      players[i].InitializeToken(playerInfo[i].playerName, playerInfo[i].tokenColor, i);
      startPosTile = gameBoardUI.GetTileAtCoord(4, 4);
      newPos.x = startPosTile.transform.localPosition.x;
      newPos.y = startPosTile.transform.localPosition.y;
      players[i].SetNewPos(newPos, 4, 4);
      startText += playerInfo[i].playerName + " Rolled : " + playerInfo[i].diceRolled.ToString() + "\n";
    }
    Destroy(gameSetupUI.gameObject);
    Destroy(gameSetupUI);
    gameState = e_game_state.roll_dice;

    turnNumber = 1;
    string firstTuirnText = "Welcome! " + players[currentPlayer].GetPlayerName() + " has the first turn. Please roll the dice";
    // Create Popup Notification Message
    DisplayTurnOrder(startText, firstTuirnText);
    // Create Dice Roll UI for first player
    CreateDiceRollUI();
  }

  // Indexes are in the tile grid 2D arrays, the Locs are the physical coordinate location
  public void TileClicked(int rowIndex, int colIndex, float xLoc, float yLoc)
  {

    UnityEngine.Debug.Log("GameHandler notified by GameBoardUI that user clicked on tile at row:"
                      + rowIndex + ", col: " + colIndex + ", StaticGameBoard tells us this is type: "
                      + gameBoardHandler.GetTileTypeByCoord(rowIndex, colIndex));

    if (gameState == e_game_state.get_player_move)
    {
      if (validMoves.Contains(new Vector2 { x = colIndex, y = rowIndex }))
      {
        currentTile = gameBoardHandler.GetTileTypeByCoord(rowIndex, colIndex);
        Vector2 newPos;
        newPos.x = xLoc;
        newPos.y = yLoc;
        Vector2 newCoord;
        newCoord.x = colIndex;
        newCoord.y = rowIndex;
        players[currentPlayer].SetNewPos(newPos, rowIndex, colIndex);
        UnityEngine.Debug.Log("Set Player" + currentPlayer + " coordinates to: " + players[currentPlayer].GetRow() + " , " + players[currentPlayer].GetCol());
        gameBoardUI.DestroyValidMoveIcons();
        // Destory RollDiceUI (it may come back if they landed on roll again though)
        rollDiceUI.Destroy();
        TileAction();
      }
    }
  }

  private void CreateDiceRollUI()
  {
    rollDiceUI = Instantiate(rollDiceUIPrefab, transform);
    rollDiceUI.rollDiceButton.onClick.AddListener(delegate { RollDice(); });
  }

  // Method for Rolling Dice
  private void RollDice()
  {
    if (gameState == e_game_state.roll_dice)
    {
      PlayerToken player = players[currentPlayer];

      if (DICE_DEBUG)
      {
        diceResult = debugDiceRolls[debugDiceIndx];
        if (debugDiceIndx < debugDiceSize)
        {
          debugDiceIndx++;
        }
        else
        {
          debugDiceIndx = 0;
        }
      }
      else
      {
        diceResult = GameHandlerUtilities.RollDice(DICE_SIDES);
      }

      rollDiceUI.UpdateResults(diceResult);
      notificationDisplay.AddLine(players[currentPlayer].GetPlayerName() + " rolled a " + diceResult);
      validMoves = gameBoardHandler.GetValidTileCoords(player.GetRow(), player.GetCol(), diceResult);
      if (validMoves.Count > 0)
      {
        gameBoardUI.ShowValidMoves(validMoves);
        gameState = e_game_state.get_player_move;
      }
    }
  }

  private void GetNewQuestion()
  {
        qDisplay.Destroy();
    if (gameState == e_game_state.get_new_question)
    {
      DrawNewCard();
      InitPlayerAnswerUI();
            // Add a listener to check the player answer
            playerAnswerUI.playerAnswerField.onEndEdit.AddListener(delegate { CheckPlayerAnswer(playerAnswerUI.GetPlayerAnswer()); });
      // Change the game state to get the player's answer
      gameState = e_game_state.get_player_answer;
          
    }
    else if (gameState == e_game_state.get_hub_question)
    {
      DrawNewCard();
      InitPlayerAnswerUI();
            // Add a listener to check the player answer
            playerAnswerUI.playerAnswerField.onEndEdit.AddListener(delegate { CheckHubAnswer(playerAnswerUI.GetPlayerAnswer()); });
      // Change the game state to get the player's answer for a hub tile
      gameState = e_game_state.get_hub_answer;
    }
  }

  // Common method for drawing a question card
  private void DrawNewCard()
  {
    // Draw a new card and update the display
    currentQuestion = gameDeck.FetchQuestionCard();
    qDisplay.SetQuestion(currentQuestion);
  }

  // Common method to create the playerAnswerUI object
  private void InitPlayerAnswerUI()
  {
    // Get the category colors
    List<Color> colors;
    colors = questionDatabase.GetCategoryColors();
    // Create the UI object as a new instance, and set the current category's color
    playerAnswerUI = Instantiate(playerAnswerUIPrefab, transform);
    playerAnswerUI.SetQuestion(currentQuestion.GetQuestionText(currentCategory), colors[(int)currentCategory]);
  }

  private void CheckPlayerAnswer(String input)
  {
    // Get the answer based on the current question and current player's tile
    UnityEngine.Debug.Log("Answer for category " + currentQuestion.GetQuestionCategory(currentCategory) + " is " + currentQuestion.GetQuestionAnswer(currentCategory));

    // If this was the correct answer we advance player and notify them
    if (currentQuestion.ValidateAnswer(currentCategory, input) || (ANS_DEBUG && input == "a"))
    {
      PlayerAnswerCorrect();
      gameBoardUI.GetComponent<GameBoardUI>().makeSound();

      // Award slice of cake if we have an HQ node
      if ((currentTile >= e_tile_type.hq_cat_1) &&
          (currentTile <= e_tile_type.hq_cat_4))
      {
        // Don't award cake twice
        if (!players[currentPlayer].CheckSpecificCake(currentCategory))
        {
          AwardPlayerCake();
        }
      }
    }
    else
    {
      PlayerAnswerIncorrect();
    }

    playerAnswerUI.Destroy();
  }

  private void CheckHubAnswer(String input)
  {
    // Get the answer based on the current question and current player's tile
    UnityEngine.Debug.Log("Answer for category " + currentQuestion.GetQuestionCategory(currentCategory) + " is " + currentQuestion.GetQuestionAnswer(currentCategory));

    // If this was the correct answer we advance player and notify them
    if (currentQuestion.ValidateAnswer(currentCategory, input) || (ANS_DEBUG && input == "a"))
    {
      gameBoardUI.GetComponent<GameBoardUI>().makeSound();
      // Player has all slices of cake (win condition)
      if (players[currentPlayer].CheckFullCake())
      {
        PlayerWon();
      }
      // Player doesn't have all slices of cake (regular hub), treat like a normal question tile
      else
      {
        PlayerAnswerCorrect();
      }
    }
    else
    {
      PlayerAnswerIncorrect();
    }

    playerAnswerUI.Destroy();
  }

  // Common method for an incorrect answer from the player
  private void PlayerAnswerIncorrect()
  {
        // Advance to the next turn, notifying the user of the change and next actions
        AdvancePlayerTurn();
    if (gameDone)
    {
      string message = "The game has ended on the first turn! The winners are: \n";
      for (int i = 0; i < 4; i++)
      {
        if (winners[i])
        {
          message += players[i].GetPlayerName() + "\n";
        }
      }
      ShowEndGamePanel(message);
      gameState = e_game_state.end_game;
    }
    else
    {
      gameState = e_game_state.roll_dice;
      NotifyMessage("Incorrect. It is now " + players[currentPlayer].GetPlayerName() + "'s turn. Roll the dice.");
    }

  }

  // Common method for a correct answer from the player
  private void PlayerAnswerCorrect()
  {
        // Notify the user that the answer was correct, and continue their turn
        NotifyMessage("Correct. It is still " + players[currentPlayer].GetPlayerName() + "'s turn. Roll again");
    gameState = e_game_state.roll_dice;
    // They need to roll again
    CreateDiceRollUI();
  }

  // Award a player a slice of cake
  private void AwardPlayerCake()
  {
    NotifyMessage(players[currentPlayer].GetPlayerName() + " obtained a piece of cake!");
    players[currentPlayer].AwardCakeSlice(currentCategory);
    players[currentPlayer].LogPlayerInfo();
  }

  // Game has been won, perform validation on turn and end the game
  private void PlayerWon()
  {
    gameState = e_game_state.roll_dice;
    int currWinner = currentPlayer;
    if (turnNumber == 1)
    {
      if (gameWon && gameDone)
      {
        // Changed output to be a common function to minimize copied code
        ShowFirstTimeWinners();
      }
      else
      {
        // Setting the winner and advancing turn should always happen regardless of the boolean status
        winners[currentPlayer] = true;
        AdvancePlayerTurn();
        string message = "Correct! " + players[currWinner].GetPlayerName();
        if (!gameWon && !gameDone)
        {
          // This player is the first player to win on their first round, inform other players that they can catch up and set gameWon
          gameWon = true;
          message += " won the game on turn 1.\n" +
              "Other players will be given chance to win. \n";
        }
        else if (gameWon && !gameDone)
        {
          // This is an additional player who won on their first round, inform them of such
          message += " has also won the game on turn 1.\n";
        }
        if (gameWon && gameDone)
        {
          ShowFirstTimeWinners();
        }
        else
        {
          message += "It is now " + players[currentPlayer].GetPlayerName() + "'s turn. Roll the dice.";
          NotifyMessage(message);
        }
      }
    }
    else
    {
      gameWon = true;
      // Notify the user that the they won and set the end state
      ShowEndGamePanel("The game has ended! " + players[currentPlayer].GetPlayerName() + " won the game!");
      gameState = e_game_state.end_game;
    }

  }

  private void ShowFirstTimeWinners()
  {
    winners[currentPlayer] = true;
    AdvancePlayerTurn();
    string message = "The game has ended on the first turn! The winners are: \n";
    for (int i = 0; i < 4; i++)
    {
      if (winners[i])
      {
        message += players[i].GetPlayerName() + "\n";
      }
    }
    ShowEndGamePanel(message);
    gameState = e_game_state.end_game;
  }

  private void MakePopup(String newline)
  {
    PopupMessage popup = Instantiate(popupMessagePrefab, transform);
    popup.SetTextforTime(newline, 3);

  }

  private void NotifyMessage(string message)
  {
    MakePopup(message);
    notificationDisplay.AddLine(message);
    UnityEngine.Debug.Log(message);
  }

  private void DisplayTurnOrder(string diceRolled, string firstTurn)
  {
    MakePopup(diceRolled + "\n" + firstTurn);
    notificationDisplay.AddLine(firstTurn);
    UnityEngine.Debug.Log(firstTurn);
  }

  private bool AdvancePlayerTurn()
  {
    try
    {
      turnDone[currentPlayer] = true;
      if (!gameWon)
      {
        currentPlayer++;
        if (currentPlayer > maxPlayer)
        {
          currentPlayer = 0;
          turnNumber++;
          for (int i = 0; i < 4; i++) { turnDone[i] = false; }
        }
        CreateDiceRollUI();
      }
      else
      {
        gameDone = true;
        for (int i = currentPlayer; i < maxPlayer + 1; i++)
        {
          if (!turnDone[i])
          {
            CreateDiceRollUI();
            gameDone = false;
            currentPlayer = i; break;
          }
        }
      }

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  // Perform game action based on the tile type
  private bool TileAction()
  {

    try
    {
      //DestroyPlayerAnswer();
      if (currentTile == e_tile_type.hub)
      {
        UnityEngine.Debug.Log("Landed on the hub node.");
        gameState = e_game_state.hub_cat_sel;
                List<Color> colors = questionDatabase.GetCategoryColors();
                qDisplay = Instantiate(qDisplayUIPrefab, transform);
                qDisplay.UpdateColors(colors);
                qDisplay.getQuestionButton.onClick.AddListener(delegate { GetNewQuestion(); });
                qDisplay.SetSelectedCategoryCallback(SelectedCategoryCallback);
                qDisplay.SetCategoryNames(
                questionDatabase.GetCategoryNames(e_question_type.cat_1),
                questionDatabase.GetCategoryNames(e_question_type.cat_2),
                questionDatabase.GetCategoryNames(e_question_type.cat_3),
                questionDatabase.GetCategoryNames(e_question_type.cat_4));
                if (players[currentPlayer].CheckFullCake())
        {
          // If the player has a full cake, their opponents choose the category
          qDisplay.DisplayCategoryOptionsForOpponents(players[currentPlayer].GetPlayerName());
        }
        else
        {
          // If the player does not have a full cake, they may choose their own category
          qDisplay.DisplayCategoryOptionsForPlayer(players[currentPlayer].GetPlayerName());
        }
      }
      else if (currentTile == e_tile_type.roll_again)
      {
        // TODO: Give a UI prompt to roll again and enforce it
        NotifyMessage(players[currentPlayer].GetPlayerName() + " has landed on roll again, please roll the dice");
        gameState = e_game_state.roll_dice;
        CreateDiceRollUI();
      }
      else if (currentTile != e_tile_type.empty)
      {

                List<Color> colors = questionDatabase.GetCategoryColors();
                qDisplay = Instantiate(qDisplayUIPrefab, transform);
                qDisplay.UpdateColors(colors);
                qDisplay.getQuestionButton.onClick.AddListener(delegate { GetNewQuestion(); });
                qDisplay.SetSelectedCategoryCallback(SelectedCategoryCallback);
                qDisplay.SetCategoryNames(
                questionDatabase.GetCategoryNames(e_question_type.cat_1),
                questionDatabase.GetCategoryNames(e_question_type.cat_2),
                questionDatabase.GetCategoryNames(e_question_type.cat_3),
                questionDatabase.GetCategoryNames(e_question_type.cat_4));
                //Instantiate question display here!!!!!!!
                gameState = e_game_state.get_new_question;
        // Tile is a HQ or question tile, so get the category and prompt for an answer
        tileQuestionMap.TryGetValue(currentTile, out currentCategory);
        NotifyMessage(players[currentPlayer].GetPlayerName() + " has moved, please draw a question card");
        // Create a player prompt ONLY if a new question is gotten
        gameState = e_game_state.get_new_question;
      }

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  // Set the category when landing on the hub node, coming from the UI object
  public void SetHubCategory(e_question_type category)
  {
    // Set the new category chosen from the UI
    currentCategory = category;
    // Destroy the category selection UI object
    // Destroy(categorySelectUI.gameObject);
    // Destroy(categorySelectUI);
    // Set game state to get the hub question
    gameState = e_game_state.get_hub_question;
  }

  // Skeleton for new SelectedCategoryCallback method
  private void SelectedCategoryCallback(e_question_type category)
  {
    UnityEngine.Debug.Log("Inside SelectedCategoryCallback");

    SetHubCategory(category);

    GetNewQuestion();
  }

  // Sets the current category
  private void SetCurrentCategory(e_question_type category)
  {
    currentCategory = category;
  }

  private void ShowEndGamePanel(string message)
  {
    endGamePanel = Instantiate(endGamePanelPrefab, transform);
    endGamePanel.endGameText.text = message;
    endGamePanel.endGameButton.onClick.AddListener(delegate { EndGame(); });
    endGamePanel.newGameButton.onClick.AddListener(delegate { NewGame(); });
  }

  private void EndGame()
  {
    Application.Quit();
  }

  private void NewGame()
  {
    SceneManager.LoadScene(0);
  }
}
