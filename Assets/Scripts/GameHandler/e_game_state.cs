﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum e_game_state 
{
  get_player_info,
  roll_dice,
  get_player_move,
  get_new_question,
  get_player_answer,
  hub_cat_sel,
  get_hub_question,
  get_hub_answer,
  end_game
}
