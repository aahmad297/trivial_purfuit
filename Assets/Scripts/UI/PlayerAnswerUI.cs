﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAnswerUI : MonoBehaviour
{
  public InputField playerAnswerField;

  private string playerAnswer;
  private bool playerIsDone;

  public Text questionText;
  public Image questionColor;

  // Constructor to set up the input text field
  void Awake()
  {
    playerAnswerField.onEndEdit.AddListener(delegate { GrabPlayerInput(); });
    playerIsDone = false;
  }

  // This function destroys the player input prompt
  public void Destroy()
  {
    playerAnswerField.onEndEdit.RemoveAllListeners();
    Destroy(this.gameObject);
    Destroy(this);
  }

  // Set the question to ask
  public void SetQuestion(string qText, Color qColor)
  {
    questionText.text = qText;
    questionColor.color = qColor;
}

  // Set the answer based on player input
  private void SetAnswer()
  {
      playerAnswer = playerAnswerField.text;
  }

  // Set the answer based on what the player enters
  public void GrabPlayerInput()
  {
    SetAnswer();
    playerIsDone = true;
    Destroy();
  }

  // Get method for whether the player is done or not
  public bool IsDone()
  {
    return playerIsDone;
  }

  // Get method for the player's answer
  public string GetPlayerAnswer()
  {
    return playerAnswer;
  }
}
