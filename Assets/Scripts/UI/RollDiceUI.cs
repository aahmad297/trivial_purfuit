﻿using UnityEngine;
using UnityEngine.UI;

public class RollDiceUI : MonoBehaviour
{
  public Button rollDiceButton;
  public Text diceResultTxt;

  // Constants
  private const string BASE_TEXT = "The result of the dice roll is: ";

  // Start is called before the first frame update
  public void Start()
  {
    // Set the button base text atnd add a listener for click events
    diceResultTxt.text = BASE_TEXT;
  }

  // Call the roll dice utility function and update the results
  public void UpdateResults(int diceResult)
  {
    UnityEngine.Debug.Log("RollDiceUI received " + diceResult + " by GameHandler as dice roll result. Displaying in UI.");

    // Update the result text field
    diceResultTxt.text = BASE_TEXT + diceResult.ToString();
  }

  // This function destroys the roll dice UI
  public void Destroy()
  {
    Destroy(this.gameObject);
    Destroy(this);
  }
}
