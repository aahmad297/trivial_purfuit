﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionDisplayUI : MonoBehaviour
{
  public Button getQuestionButton;
  public Text[] questionText;
  public Image[] categoryColors;
  public Color[] categoryColorTypes;
  private string[] catNames;

  private QuestionCard currentCard;

  public CategorySelector categorySelectionPrefab;
  private CategorySelector categorySelection;
  private Action<e_question_type> SelectedCategoryCallback;

  void Awake()
  {
    catNames = new string[4];
    categoryColorTypes = new Color[4];
  }
 public void Destroy()
    {
        Destroy(this.gameObject);
        Destroy(this);
    }
  public void SetQuestion(QuestionCard newQuestion)
  {
    currentCard = newQuestion;
    UpdateQuestionDisplay();
    return;
  }

  public void SetCategoryNames(string cat1, string cat2, string cat3, string cat4)
  {
    catNames[0] = cat1;
    catNames[1] = cat2;
    catNames[2] = cat3;
    catNames[3] = cat4;
  }

  public void SetSelectedCategoryCallback(Action<e_question_type> selectedCategoryCallback)
  {
    SelectedCategoryCallback = selectedCategoryCallback;
  }

  private void UpdateQuestionDisplay()
  {
    for (int i = 0; i < Enum.GetNames(typeof(e_question_type)).Length; i++)
    {
      questionText[i].text = currentCard.GetQuestionText((e_question_type)i);
    }
  }

  public void UpdateColors(List<Color> colors)
  {
    int i = 0;

    // Foreach is the easiest way to loop through the list
    foreach (Color color in colors)
    {
      // Provide a check to throw away colors if more than 4 are entered
      if (i < categoryColors.Length)
      {
        categoryColorTypes[i] = color;
        categoryColors[i++].color = color;
      }
    }
  }

  public QuestionCard GetCurrentCard()
  {
    return currentCard;
  }

  private void CreateCategorySelectionElement(string text, Action<e_question_type> selectedCategoryCallback)
  {
    // Create the Category Selection display
    categorySelection = Instantiate(categorySelectionPrefab, transform.parent);
    categorySelection.SetText(text);
    categorySelection.SetCallbackFunction(selectedCategoryCallback);
    categorySelection.SetCategoryColors(categoryColorTypes);
    categorySelection.SetCategoryNames(catNames);
  }

  // Shell for the DisplayCategoryOptionsForOpponents method
  public void DisplayCategoryOptionsForOpponents(string playerName)
  {
    CreateCategorySelectionElement(playerName + 
      " only needs one more correct answer to win! " +
      "Players: select their final question category", 
      UICategorySelectionCallback);
  }

  // Shell for the DisplayCategoryOptionsForPlayer method
  public void DisplayCategoryOptionsForPlayer(string playerName)
  {
    CreateCategorySelectionElement(playerName + 
      ", you may select your question category " +
      "for landing on the hub tile",
      UICategorySelectionCallback);
  }

  private void UICategorySelectionCallback(e_question_type category)
  {
    // First destroy the UI element
    Destroy(categorySelection.gameObject);
    Destroy(categorySelection);

    // then finish the callback, callback into the GameHandler
    SelectedCategoryCallback(category);
  }

}
