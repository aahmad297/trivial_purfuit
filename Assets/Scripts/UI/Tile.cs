﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Tile : MonoBehaviour
{
  public Image tileColor;
  public Image cakeColor;
  public Text tileText;
  public Image hubCrown;
  public int rowIndex, columnIndex;
  private Dictionary<e_tile_type, Color> colorMap;
  private Action<int, int, float, float> clickCallBack;

  // TODO: We'll need tileType to know what text to put on, or we
  // can have the  GameHandler just set the text, no need to worry now
  // private e_tile_type tileType;

  public void Start()
  {
    EventTrigger.Entry entry = new EventTrigger.Entry();
    entry.eventID = EventTriggerType.PointerClick;
    entry.callback.AddListener((eventData) => { ClickTile(); });
    this.GetComponent<EventTrigger>().triggers.Add(entry);
    UnityEngine.Debug.Log("Added Event Trigger");
  }

  public void ClickTile()
  {
    clickCallBack(rowIndex, columnIndex, transform.localPosition.x, transform.localPosition.y);
  }

  public bool SetColorMap(Dictionary<e_tile_type, Color> NewColorMap)
  {
    try
    {
      colorMap = NewColorMap;
      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  public void SetClickCallback(Action<int, int, float, float> clickCallBackFunc)
  {
    clickCallBack = clickCallBackFunc;
  }

  public bool SetArrayIndices(int row, int column)
  {
    try
    {
      rowIndex = row;
      columnIndex = column;

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  public void SetTileColor(Color color)
  {
    tileColor.color = color;
    cakeColor.color = color;
  }

  public void SetCakeVisibleAndColor(Color color1) {
    cakeColor.color = color1;
  }

  public void SetTileText(string text) {
    tileText.text = text;
  }

  public void SetHubCrown(bool enabled) {
    hubCrown.enabled = enabled;
  }
}
