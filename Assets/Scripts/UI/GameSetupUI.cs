﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public struct s_player_info { public string playerName; public Color tokenColor; public int diceRolled; }
public class GameSetupUI : MonoBehaviour
{

  public GameObject numSel;
  public Button twoPlayer;
  public Button threePlayer;
  public Button fourPlayer;
  public int numPlayers;

  public GameSetupInfoUI playerInfoScreenPrefab;
  public GameSetupInfoUI playerInfoScreen;
  private int infoGrabbedNum;
  public s_player_info[] playerInfo;
  private bool[] colorsUsed = { false, false, false, false };
  private List<Color> colors = new List<Color>();


  // Start is called before the first frame update
  void Start()
  {
    numSel.gameObject.SetActive(true);
    twoPlayer.onClick.AddListener(delegate { SetNumPlayers(2); });
    threePlayer.onClick.AddListener(delegate { SetNumPlayers(3); });
    fourPlayer.onClick.AddListener(delegate { SetNumPlayers(4); });
  }

  // Update is called once per frame
  void Update()
  {
        
  }

  public void SetColors(List<Color> databaseColors)
  {
    colors = databaseColors;
  }

  private void SetNumPlayers(int players)
  {
    numPlayers = players;
    playerInfo = new s_player_info[numPlayers];
    numSel.gameObject.SetActive(false);
    infoGrabbedNum = 0;
    SetupPlayerInfo();
  }

  private void NextPlayerInfo()
  {
    Destroy(playerInfoScreen.gameObject);
    Destroy(playerInfoScreen);
    SetupPlayerInfo();
  }

  private void SetupPlayerInfo()
  {
    playerInfoScreen = Instantiate(playerInfoScreenPrefab, transform);
    playerInfoScreen.InitializeScreen(infoGrabbedNum + 1, colorsUsed);
    for (int i = 0; i < 4; i++)
    {
      playerInfoScreen.colorButtons[i].image.color = colors[i];
      playerInfoScreen.colorButtons[i].onClick.AddListener(delegate { SetPlayerInfo(); });
    }
  }

  private void SetPlayerInfo()
  {
    if (playerInfoScreen.playerNameField.text != "")
    {
      playerInfo[infoGrabbedNum].playerName = playerInfoScreen.playerNameField.text;
      playerInfo[infoGrabbedNum].tokenColor = playerInfoScreen.colorSelected;

      playerInfo[infoGrabbedNum].diceRolled = 0;
      bool invalidRoll = true;

      int roll = UnityEngine.Random.Range(1, 7);

      while (invalidRoll)
      {
        roll = UnityEngine.Random.Range(1, 7);
        invalidRoll = false;
        for (int i=0; i<infoGrabbedNum; i++)
        {
          if (playerInfo[i].diceRolled == roll) { invalidRoll = true; }
        }
      }

      playerInfo[infoGrabbedNum].diceRolled = roll;
      colorsUsed[playerInfoScreen.colorSelectedNum] = true;
      infoGrabbedNum++;
      
      if (infoGrabbedNum < numPlayers)
      {
        NextPlayerInfo();
      }
      else
      {
        Array.Sort<s_player_info>(playerInfo, (x, y) => y.diceRolled.CompareTo(x.diceRolled));
        transform.parent.GetComponent<GameHandler>().InitPlayerTokens();
      }

    }
  }


}
