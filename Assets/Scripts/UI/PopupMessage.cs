﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupMessage : MonoBehaviour
{
    void Awake()
    {
        this.enabled = false;
    }

    public void SetText(string newLine)
    {
        this.GetComponent<Text>().text = newLine;
    }

    private void DisableAfterTime()
    {
        Text textBox = null;
        Transform[] Temp = this.GetComponentsInChildren<Transform>();
        foreach (Transform Child in Temp)
        {
            if (Child.name == "PopupMessageText") { textBox = Child.gameObject.GetComponent<Text>(); }
        }

        Image image = this.GetComponent<Image>();

        image.enabled = false;
        textBox.enabled = false;
    }

    public void DestroyAfterTime()
    {
        Destroy(this.gameObject);
        Destroy(this);
    }

    public void SetTextforTime(string newLine, int seconds)
    {
        Text textBox = null;
        Transform[] Temp = this.GetComponentsInChildren<Transform>();
        foreach (Transform Child in Temp)
        {
            if (Child.name == "PopupMessageText") { textBox = Child.gameObject.GetComponent<Text>(); }
        }

        textBox.text = newLine;

        //Image image = this.GetComponent<Image>();
        //image.enabled = true;
        //textBox.enabled = true;
        //Invoke("DisableAfterTime", 3);
        Invoke("DestroyAfterTime",seconds);
    }

}
