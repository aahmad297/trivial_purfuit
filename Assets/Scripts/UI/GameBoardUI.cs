﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameBoardUI : MonoBehaviour
{
  public Tile tile_prefab;
  public GameObject validTilePrefab;
  private Action<int, int, float, float> clickCallBack;
  private GameHandler gameHandler;
  private List<Tile> tiles;
  private List<GameObject> validTileIcons;

  public void Initialize(Action<int, int, float, float> clickCallBackFunc)
  {
    clickCallBack = clickCallBackFunc;
  }

  // TODO: nice comments everywhere
  // TileFormat contains all the types for the grid, row 0 
  // is the top row of the board we want to make, since everytime
  // we add a new tile it adds it to the right from the bottom, 
  // the first tile we create is bottom left, last tile we make is
  // top right
  public void Populate(e_tile_type[,] TileFormat, Dictionary<e_tile_type, Color> ColorMap)
  {
    Tile newTile;
    tiles = new List<Tile>();
    Vector2 tilePos;
    Destroy(this.GetComponent<GridLayoutGroup>());
    // Start from bottom row
    for (int row = TileFormat.GetLength(0) - 1; row >= 0; row--)
    {
      // Start from furthest left column
      for (int col = 0; col < TileFormat.GetLength(1); col++)
      {
        // Create new instance of the tile prefab
        newTile = Instantiate(tile_prefab, transform);

        e_tile_type tileType = TileFormat[row, col];
        newTile.GetComponent<Tile>().SetTileColor(ColorMap[tileType]);
        
        if(tileType >= e_tile_type.hq_cat_1 && tileType <= e_tile_type.hq_cat_4) {
          newTile.GetComponent<Tile>().SetCakeVisibleAndColor(ColorMap[tileType - 5]);
        }

        newTile.GetComponent<Tile>().SetArrayIndices(row, col);
        newTile.GetComponent<Tile>().SetClickCallback(clickCallBack);

        if(tileType == e_tile_type.roll_again) {
          newTile.GetComponent<Tile>().SetTileText("Roll\nAgain");
        }

        newTile.GetComponent<Tile>().SetHubCrown(tileType == e_tile_type.hub);

        tilePos.x = col * 100 - 400;
        tilePos.y = row * 100 - 400;
        newTile.transform.localPosition = tilePos;
        tiles.Add(newTile);
      }
    }

        GetComponent<AudioSource>().Play();

    
  }
   public void makeSound()
    {
        GetComponent<AudioSource>().Play();

    }

    // TODO: Probably delete this if it's not needed
    public bool SetGameHandler(GameHandler newGameHandler)
  {
    try
    {
      gameHandler = newGameHandler;
      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  public List<Tile> GetBoardTileRef()
  {
    return tiles;
  }

  public Tile GetTileAtCoord(int colInd, int rowInd)
  {
    return tiles.Find(x => ((x.rowIndex == rowInd) && (x.columnIndex == colInd)));
  }

  public void ShowValidMoves(List<Vector2> validMoves)
  {
    validTileIcons = new List<GameObject>();
    GameObject validTileIcon;
    Tile validTile;
    for (int i = 0; i < validMoves.Count; i++)
    {
      UnityEngine.Debug.Log("Valid Coordinates: " + (int)validMoves[i].x + " , " + (int)validMoves[i].y);

      validTileIcon = Instantiate(validTilePrefab, transform);
      validTile = GetTileAtCoord((int)validMoves[i].x, (int)validMoves[i].y);
      validTileIcon.transform.localPosition = validTile.transform.localPosition;
      validTileIcons.Add(validTileIcon);
    }
  }

  public void DestroyValidMoveIcons()
  {
    GameObject currentIcon;
    int count = validTileIcons.Count;
    for (int i = 0; i < count; i++)
    {
      currentIcon = validTileIcons[0];
      validTileIcons.RemoveAt(0);
      Destroy(currentIcon);
    }
  }

}
