﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CategorySelectUI : MonoBehaviour
{
  public Button[] categoryButtons;
  public Text displayText;


  // Constructor to set up the buttons
  void Awake()
  {
    try
    {
      // TODO: Actually set these up and link to GUI
      // displayText.text = "Change me! Currently Hardcoded";
      // for (int i = 0; i < categoryButtons.Length; i++)
      // {
      //   categoryButtons[i].onClick.AddListener(delegate { SetCategory((e_question_type)(i)); });
      // }
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
    }
  }

  // Set the question category, based on button click
  private void SetCategory(e_question_type chosenCategory)
  {
    transform.parent.GetComponent<GameHandler>().SetHubCategory(chosenCategory);
  }
}
