﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationDisplay : MonoBehaviour
{
    public NotificationText textPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ScrollToBottom()
    {
        ScrollRect scrollRect = this.GetComponent<ScrollRect>();

        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0f;
        Canvas.ForceUpdateCanvases();
    }

    public bool AddLine(string newLine)
    {
        RectTransform RetVal = null;
        Transform[] Temp = this.GetComponentsInChildren<Transform>();
        foreach (Transform Child in Temp)
        {
            if (Child.name == "Content") { RetVal = Child.gameObject.GetComponent<RectTransform>(); }
        }

        GridLayoutGroup gridlayoutgroup = this.GetComponent<GridLayoutGroup>();

        NotificationText newText = Instantiate(textPrefab, transform);
        newText.transform.SetParent(RetVal.transform);
        newText.SetText(newLine);

        ScrollToBottom();

        // TODO: Have size limits / figure out best size for this when we know
        // what we're logging
        return true;
    }

}
