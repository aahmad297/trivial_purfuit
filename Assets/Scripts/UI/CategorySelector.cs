﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class CategorySelector : MonoBehaviour
{
  public Text promptText;
  public Text[] categoryText;
  public Image[] categoryButtonColor;
  public Button[] categoryButtons;
  public e_question_type categorySelected;
  private Action<e_question_type> categorySelectCallback;

  // Start is called before the first frame update
  void Start()
  {
    categoryButtons[0].onClick.AddListener(delegate { SelectCategory(e_question_type.cat_1); });
    categoryButtons[1].onClick.AddListener(delegate { SelectCategory(e_question_type.cat_2); });
    categoryButtons[2].onClick.AddListener(delegate { SelectCategory(e_question_type.cat_3); });
    categoryButtons[3].onClick.AddListener(delegate { SelectCategory(e_question_type.cat_4); });
  }

  public void SetCategoryNames(string[] names)
  {
    for (int idx = 0; idx < 4; idx++)
    {
      categoryText[idx].text = names[idx];
    }
  }

  public void SetCategoryColors(Color[] categoryColors)
  {
    for (int idx = 0; idx < 4; idx++)
    {
      categoryButtonColor[idx].color = categoryColors[idx];
    }
  }

  public void SetText(string text)
  {
    promptText.text = text;
  }

  public void SetCallbackFunction(Action<e_question_type> categorySelectCallbackFunc)
  {
    categorySelectCallback = categorySelectCallbackFunc;
  }

  // When the button has selected a category, call the GameHandler's
  // callback function so it knows what category was selected
  private void SelectCategory(e_question_type catgeory)
  {
    if (categorySelectCallback == null)
    {
      UnityEngine.Debug.Log("No category selected function");
      return;
    }
    categorySelectCallback(catgeory);
  }
}
