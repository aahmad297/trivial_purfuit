﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerToken : MonoBehaviour
{
  //Variable if any flag should be activated
  [SerializeField] private bool[] cakeSlices = { false, false, false, false };
  [SerializeField] private Image[] cakeSliceUI;
  private Dictionary<e_question_type, Color> cakeColors;

  private int rowIndex;
  private int colIndex;

  private int playerNum;
  private Vector2 posOffset;
  //token color and player name
  private string playerName;

    //Images associated with the token

  private void Awake()
  {
    //All flags off at start
    NoCake();

    //newPos(new Vector2(345, 418));
    SetNewPos(new Vector2(0, 0), 4, 4);
    cakeColors = new Dictionary<e_question_type, Color>();
  }

  // For demo purposes
  public bool LogPlayerInfo()
  {
    try
    {
      UnityEngine.Debug.Log("[PlayerToken] Info: \tName: " + playerName +
                            "\n\t\t\t\tNumber Slices: " + GetNumSlices());
      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  //turn off all cake - could be used on start
  private bool NoCake()
  {
    try
    {
      for (int i = 0; i < 4; i++)
      {
        cakeSliceUI[i].enabled = false;
      }

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  ////set new location to go to on board
  public bool SetNewPos(Vector2 newSpot, int nRowIndex, int nColIndex)
  {
    try
    {
      this.transform.localPosition = newSpot + posOffset;
      rowIndex = nRowIndex;
      colIndex = nColIndex;

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  ////return x,y position token is in
  public int GetRow()
  {
    return rowIndex;
  }

  public int GetCol()
  {
    return colIndex;
  }

  ////set player name
  public bool SetPlayerName(string newName)
  {
    try
    {
      playerName = newName;
      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  ////get player name
  public string GetPlayerName()
  {
    return playerName;
  }

  // Return color of token
  public Color GetTokenColor()
  {
    return this.gameObject.GetComponent<Image>().color;
  }

  // Initialize a token by setting its color and player name
  public bool InitializeToken(string playerName, Color tokenColor, int playerNum)
  {
    try
    {
      this.gameObject.GetComponent<Image>().color = tokenColor;
      SetPlayerName(playerName);
      switch (playerNum)
      {
        case 0:
          posOffset = new Vector2(-25, 25);
          break;
        case 1:
          posOffset = new Vector2(25, 25);
          break;
        case 2:
          posOffset = new Vector2(-25, -25);
          break;
        case 3:
          posOffset = new Vector2(25, -25);
          break;
      }
      return true;

    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  ////return number of cake slices
  public int GetNumSlices()
  {
    int numSlices = 0;
    for (int i = 0; i<4; i++)
    {
      if (cakeSlices[i] == true) { numSlices++; }
    }
    return numSlices;
  }

  // Award a slice of cake and set its color
  public int AwardCakeSlice(e_question_type cakeType)
  {
    int cakeIndx = (int)cakeType;
    if (cakeSlices[cakeIndx])
    {
      UnityEngine.Debug.Log("[PlayerToken] Category type: " + cakeType  + " has already been awarded");
    }
    else
    {
      UnityEngine.Debug.Log("[PlayerToken] Awarding cake slice for category type: " + cakeType);
    }
    cakeSlices[cakeIndx] = true;
    cakeColors.TryGetValue(cakeType, out Color sliceColor);
    cakeSliceUI[cakeIndx].color = sliceColor;
    cakeSliceUI[cakeIndx].enabled = true;
    return GetNumSlices();
  }

  // Set the cake colors
  public bool SetCakeColors(List<Color> colors)
  {
    try
    {
      int i = 0;

      // Foreach is the easiest way to loop through the list
      foreach (Color color in colors)
      {
        cakeColors.Add((e_question_type)(i++), color);
      }

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  // Check if the player has all pieces of cake
  public bool CheckFullCake()
  {
    bool fullCake = true;

    foreach (bool cake in cakeSlices)
    {
      fullCake &= cake;
    }

    return fullCake;
  }

  // Check if the player has a specific cake slice
  public bool CheckSpecificCake(e_question_type cakeCategory)
  {
    try
    {
      return cakeSlices[(int)(cakeCategory)];
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }
}