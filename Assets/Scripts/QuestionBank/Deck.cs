﻿using System;
using System.Collections.Generic;
using QuestionExtensionMethods;

public class Deck
{
  private List<QuestionCard> questionDeck;
  private List<QuestionCard> discardPile;

  public Deck(List<QuestionCard> startDeck)
  {
    questionDeck = new List<QuestionCard>();
    discardPile  = new List<QuestionCard>();
    SetupDeck(startDeck);
    return;
  }

  private bool SetupDeck(List<QuestionCard> startDeck) 
  {
    try
    {
      questionDeck = startDeck;
      UnityEngine.Debug.Log("The deck and discard piles have " + questionDeck.Count + " and " + discardPile.Count + " cards respectively");
      Shuffle();

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  private bool CanFetchCard()
  {
    return questionDeck.Count != 0;
  }

  public QuestionCard FetchQuestionCard()
  {
    QuestionCard tQuestion = null;

    if (!CanFetchCard())
    {
      UnityEngine.Debug.Log("Reusing Discard Pile...");
      ReuseDiscardPile();
    } 

    tQuestion = questionDeck[0];
    discardPile.Add(tQuestion);
    questionDeck.Remove(tQuestion);
    UnityEngine.Debug.Log("The deck and discard piles have " + questionDeck.Count + " and " + discardPile.Count + " cards respectively");
    
    for (int i = 0; i < 4; i++)
    {
      UnityEngine.Debug.Log("New Question Card's " + tQuestion.GetQuestionCategory((e_question_type)i) +
        " category answer is " + tQuestion.GetQuestionAnswer((e_question_type)i) + ".");
    }

    return tQuestion;
  }

  private bool DiscardQuestion(QuestionCard dQuestion)
  {
    try
    {
      discardPile.Insert(0, dQuestion);

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  private bool Shuffle()
  {
    try
    {
      questionDeck.ShuffleList();

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  private bool ReuseDiscardPile() 
  {
    try
    {
      questionDeck = discardPile;
      discardPile = new List<QuestionCard>();
      Shuffle();

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }
}
