﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace QuestionExtensionMethods
{
    public static class QuestionListExtension
    {
        /** From https://stackoverflow.com/questions/49570175/simple-way-to-randomly-shuffle-list
        *  Based on Fisher–Yates shuffle algorithm
        */
        public static void ShuffleList<T>(this IList<T> list)  
        {  
            Random random = new Random();  
            int n = list.Count;  

            for(int i= list.Count - 1; i > 1; i--)
            {
                int rnd = random.Next(i + 1);  

                T value = list[rnd];  
                list[rnd] = list[i];  
                list[i] = value;
            }
        }
    }
}
