﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Question Card", menuName = "Question Card")]
public class QuestionCard : ScriptableObject
{
  [LabeledArray(typeof(e_question_type))]
  [TextArea(5, 20)]
  [SerializeField] public string[] questions  = {"", "", "", ""};

  [LabeledArray(typeof(e_question_type))]
  [TextArea(5, 20)]
  [SerializeField] public string[] answers = { "", "", "", "" };

  [LabeledArray(typeof(e_question_type))]
  [TextArea(5, 20)]
  [SerializeField] public string[] categories = { "", "", "", "" };

  public void SetQnAByType(e_question_type qType, string question, string answer) 
  {
    int indx = (int)qType;
    questions[indx] = question;
    answers[indx] = answer;
    return;
  }

  public void SetQByType(e_question_type qType, string question)
  {
    int indx = (int)qType;
    questions[indx] = question;
    return;
  }

  public void SetAByType(e_question_type qType, string answer)
  {
    int indx = (int)qType;
    answers[indx] = answer;
    return;
  }

  public void SetCategoryByType(e_question_type qType, string catName)
  {
    int indx = (int)qType;
    categories[indx] = catName;
    return;
  }

  public string GetQuestionText(e_question_type qType)
  {
    int indx = (int)qType;
    return questions[indx];
  }

  public bool ValidateAnswer(e_question_type qType, string answer) 
  {
    string correctAnswer = GetQuestionAnswer(qType);
    int distance = LevenshteinDistance(answer, correctAnswer);
    // 3 if < 10, otherwise use 5
    int allowed_distance = 0;
    if (correctAnswer.Length > 5)
    {
      allowed_distance = 2;
    }
    if (correctAnswer.Length > 10)
    {
      allowed_distance = 4;
    }
    if (distance < allowed_distance)
    {
      return true;
    }
    return false;
  }

  public string GetQuestionAnswer(e_question_type qType)
  {
    int indx = (int)qType;
    return answers[indx];
  }

  public string GetQuestionCategory(e_question_type qType)
  {
    int indx = (int)qType;
    return categories[indx];
  }

  // Code Taken Directly from: https://www.csharpstar.com/csharp-string-distance-algorithm/
  public static int LevenshteinDistance(string s, string t)
  {
    int n = s.Length;
    int m = t.Length;
    int[,] d = new int[n + 1, m + 1];

    // Step 1
    if (n == 0)
    {
      return m;
    }

    if (m == 0)
    {
      return n;
    }

    // Step 2
    for (int i = 0; i <= n; d[i, 0] = i++)
    {
    }

    for (int j = 0; j <= m; d[0, j] = j++)
    {
    }

    // Step 3
    for (int i = 1; i <= n; i++)
    {
      //Step 4
      for (int j = 1; j <= m; j++)
      {
        // Step 5
        int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

        // Step 6
        d[i, j] = Math.Min(
            Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
            d[i - 1, j - 1] + cost);
      }
    }
    // Step 7
    return d[n, m];
  }

}
