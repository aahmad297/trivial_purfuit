﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameBoardHandler : MonoBehaviour
{
  private Dictionary<e_tile_type, Color> colorMap;
  private e_tile_type[,] tileInfoArray;
  private List<Tile> boardTiles;

  // Awake is called when you instantiate it
  private void Awake()
  {
    colorMap = new Dictionary<e_tile_type, Color>();
    // GameHandler can later set the colors to be something else
    //SetDefaultColorsForTiles();
    DefineBoardLayout();
  }

  public e_tile_type[,] GetTypeArray()
  {
    return tileInfoArray;
  }

  public Dictionary<e_tile_type, Color> GetColorMap()
  {
    return colorMap;
  }

  public bool SetColorsForTiles(List<Color> colors)
  {
    try
    {
      int i = 0;
      Color baseColor = Color.yellow;
      if(colors.Exists(c => Vector4.Distance((Vector4) c, (Vector4) baseColor) < 0.10))
      {
        Color orange = new Color32(255, 137, 54, 255);
        Color coral = new Color32(52, 235, 171, 255);
        Color lightBlue = new Color32(28, 196, 252, 255);
        baseColor = !colors.Exists(c => Vector4.Distance((Vector4) c, (Vector4) orange) < 0.10) ? orange :
        !colors.Exists(c => Vector4.Distance((Vector4) c, (Vector4) coral) < 0.10) ? coral :
        !colors.Exists(c => Vector4.Distance((Vector4) c, (Vector4) lightBlue) < 0.10) ? lightBlue :
        Color.grey;
      }


      // Foreach is the easiest way to loop through the list
      foreach (Color color in colors)
      {
        // Only handle 4 colors
        if (i < GameHandler.MAX_PLAYERS)
        {
          // Set for the regular tiles (enum indexes 0-3)
          colorMap.Add((e_tile_type)(i), colors[i]);

          // Set for the hq tiles (enum indexes 5-8)
          colorMap.Add((e_tile_type)(i + 5), baseColor);
          i++;
        }
      }

      // Constant colors
      colorMap.Add(e_tile_type.hub, baseColor);
      colorMap.Add(e_tile_type.empty, Color.clear);
      colorMap.Add(e_tile_type.roll_again, baseColor);

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  private bool SetDefaultColorsForTiles()
  {
    try
    {
      List<Color> defaultColors = new List<Color>
      {
        Color.red,
        Color.white,
        Color.blue,
        Color.green
      };

      SetColorsForTiles(defaultColors);

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  private bool DefineBoardLayout()
  {
    try
    {
      tileInfoArray = new e_tile_type[9, 9]
      {
      { e_tile_type.cat_2,      e_tile_type.cat_4, e_tile_type.roll_again, e_tile_type.cat_3, e_tile_type.hq_cat_1, e_tile_type.cat_3, e_tile_type.roll_again, e_tile_type.cat_2, e_tile_type.cat_3},
      { e_tile_type.cat_1,      e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_3,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_1},
      { e_tile_type.roll_again, e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_2,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.roll_again },
      { e_tile_type.cat_2,      e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_4,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_4 },
      { e_tile_type.hq_cat_4,   e_tile_type.cat_2, e_tile_type.cat_1,      e_tile_type.cat_3, e_tile_type.hub,      e_tile_type.cat_1, e_tile_type.cat_3,      e_tile_type.cat_4, e_tile_type.hq_cat_2 },
      { e_tile_type.cat_2,      e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_2,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_4 },
      { e_tile_type.roll_again, e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_4,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.roll_again },
      { e_tile_type.cat_3,      e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_1,    e_tile_type.empty, e_tile_type.empty,      e_tile_type.empty, e_tile_type.cat_3 },
      { e_tile_type.cat_1,      e_tile_type.cat_4, e_tile_type.roll_again, e_tile_type.cat_1, e_tile_type.hq_cat_3, e_tile_type.cat_1, e_tile_type.roll_again, e_tile_type.cat_2, e_tile_type.cat_4 },
      };

      return true;
    }
    catch (Exception exception)
    {
      UnityEngine.Debug.LogError(exception);
      return false;
    }
  }

  public e_tile_type GetTileTypeByCoord(int xCoord, int yCoord)
  {
    return tileInfoArray[xCoord, yCoord];
  }

  // Check to see if the player is on the center, or hub, tile
  public bool PlayerOnCenter(int row, int col)
  {
    return tileInfoArray[row, col] == e_tile_type.hub;
  }

  public void GrabTiles(List<Tile> tiles)
  {
    boardTiles = tiles;
  }

  // Unnecessarily convoluted recurrsive function that grabs all valid tiles
  private enum e_search_dir { up, down, left, right }; 
  private List<Tile> GetValidTiles(int playerRow, int playerCol, int numMoves, e_search_dir dir)
  {
    int col = playerCol;
    int row = playerRow;
    List<Tile> validTiles = new List<Tile>();
    bool tileValid = true;

    for (int i = numMoves; i > 0; i--)
    {
      if (dir == e_search_dir.up)
      {
        if (row < 8)
        {
          row++;
          if (((row == 4) || (row == 8)) && (col == 0))
          {
            validTiles.AddRange(GetValidTiles(row, col, i-1, e_search_dir.right));
          } 
          else if (((row == 4) || (row ==8)) && (col == 4))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.right));
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.left));
          }
          else if (((row == 4) || (row == 8)) && (col == 8))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.left));
          }
        }
        else
        {
          tileValid = false;
          i = 0;
        }
      } 
      else if (dir == e_search_dir.down)
      {
        if (row > 0)
        {
          row--;
          if (((row == 0) || (row == 4)) && (col == 0))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.right));
          }
          else if (((row == 0) || (row == 4)) && (col == 4))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.right));
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.left));
          }
          else if (((row == 0) || (row == 4)) && (col == 8))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.left));
          }
        }
        else
        {
          tileValid = false;
          i = 0;
        }
      }
      else if (dir == e_search_dir.left)
      {
        if (col > 0)
        {
          col--;
          if (((col == 0) || (col == 4)) && (row == 0))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.up));
          }
          else if (((col == 0) || (col == 4)) && (row == 4))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.up));
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.down));
          }
          else if (((col == 0) || (col == 4)) && (row == 8))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.down));
          }
        }
        else
        {
          tileValid = false;
          i = 0;
        }
      }
      else if (dir == e_search_dir.right)
      {
        if (col < 8)
        {
          col++;
          if (((col == 4) || (col == 8)) && (row == 0))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.up));
          }
          else if (((col == 4) || (col == 8)) && (row == 4))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.up));
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.down));
          }
          else if (((col == 4) || (col == 8)) && (row == 8))
          {
            validTiles.AddRange(GetValidTiles(row, col, i - 1, e_search_dir.down));
          }
        }
        else
        {
          tileValid = false;
          i = 0;
        }
      }

      if (tileInfoArray[row, col] == e_tile_type.empty)
      {
        tileValid = false;
        i = 0;
      }
    }

    if (tileValid)
    {
      validTiles.Add(boardTiles.Find(x => ((x.rowIndex == row) && (x.columnIndex == col))));
    }
    return validTiles;
  }


  // Grab all valid tiles and 
  public List<Vector2> GetValidTileCoords(int playerRow, int playerCol, int numMoves)
  {
    List<Tile> validTiles = new List<Tile>(); 
    validTiles.AddRange(GetValidTiles(playerRow, playerCol, numMoves, e_search_dir.up));
    validTiles.AddRange(GetValidTiles(playerRow, playerCol, numMoves, e_search_dir.down));
    validTiles.AddRange(GetValidTiles(playerRow, playerCol, numMoves, e_search_dir.left));
    validTiles.AddRange(GetValidTiles(playerRow, playerCol, numMoves, e_search_dir.right));

    List<Vector2> validPos = new List<Vector2>();
    Vector2 currPos;
    for (int i = 0; i<validTiles.Count; i++)
    {
      currPos.x = validTiles[i].columnIndex;
      currPos.y = validTiles[i].rowIndex;
      validPos.Add(currPos);
    }

    return validPos;
  }


}
