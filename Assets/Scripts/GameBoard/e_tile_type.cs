﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum e_tile_type
{
  cat_1,
  cat_2,
  cat_3,
  cat_4,
  roll_again,
  hq_cat_1,
  hq_cat_2,
  hq_cat_3,
  hq_cat_4,
  hub,
  empty
}