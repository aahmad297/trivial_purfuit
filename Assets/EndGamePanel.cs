﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePanel : MonoBehaviour
{

  public Button newGameButton;
  public Button endGameButton;
  public Text endGameText;

}
