# Trivial Purfuit

This Clash of CLANZ's bitbucket repository for Foundations of Software's Trivial Purfuit class Project

This repository is managed by the following people:
1. Corey Bailey
2. Lauren Wood
3. Ali Ahmad
4. Nikhil Shah
5. Zach Deering

---

## Getting Started

### Software Version
1. `Unity 2019.3.12f1` : The game is developed within the Unity Game Engine
2. `Visual Studio` : Visual Studio Community 2017
3. `UniGit`: Used for Unity repository management. https://github.com/simeonradivoev/UniGit

Ensure that at least the above software and packages are installed on your machine before getting started.

TBD

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
